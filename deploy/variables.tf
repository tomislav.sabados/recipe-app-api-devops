variable "prefix" {
  default = "raad"
}
variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "email@sabados.com"
}

variable "db_username" {
  description = "Username for RDS postgres instance"
}

variable "db_password" {
  description = "Password for RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ecs image fro api"
  default     = "760624446363.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ecs image fro proxy"
  default     = "760624446363.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "django secret key"
}